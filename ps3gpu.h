/*-
 * Copyright (C) 2011, 2012 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#ifndef _PS3GPU_H
#define _PS3GPU_H

#define PS3GPU_MEMORY_GART_ADDRESS_OFFSET	0x00000000ul

enum ps3gpu_memory_type {
	PS3GPU_MEMORY_TYPE_VIDEO,
	PS3GPU_MEMORY_TYPE_GART,
	PS3GPU_MEMORY_TYPE_REGISTER
};

struct ps3gpu_memory {
	TAILQ_ENTRY(ps3gpu_memory) m_queue;

	struct ps3gpu_context *m_context;

	int m_type;
	int m_start;
	int m_size;

	unsigned long m_paddr;
	unsigned long m_vaddr;
	unsigned int m_gaddr;
};

TAILQ_HEAD(ps3gpu_memory_queue, ps3gpu_memory);

struct ps3gpu_device;
struct ps3gpu_user;

#define PS3GPU_CONTEXT_MAX_HEAPS	2

struct ps3gpu_context {
	struct ps3gpu_device *ctx_dev;

	int ctx_id;

	int ctx_vram_size;

	unsigned long ctx_memory_handle;

	unsigned long ctx_vram_paddr;
	unsigned long ctx_vram_vaddr;

	unsigned long ctx_handle;

	unsigned long ctx_control_paddr;
	unsigned long ctx_control_vaddr;
	unsigned long ctx_control_size;

	unsigned long ctx_driver_info_paddr;
	unsigned long ctx_driver_info_vaddr;
	unsigned long ctx_driver_info_size;

	unsigned long ctx_reports_paddr;
	unsigned long ctx_reports_vaddr;
	unsigned long ctx_reports_size;

	struct sx ctx_lock;

	struct ps3gpu_heap ctx_heap[PS3GPU_CONTEXT_MAX_HEAPS];
	struct ps3gpu_memory_queue ctx_mem_queue;

	struct ps3gpu_user *ctx_user;

	TAILQ_ENTRY(ps3gpu_context) ctx_user_queue;

	struct ps3gpu_memory *ctx_control_mem;
	struct ps3gpu_memory *ctx_driver_info_mem;
	struct ps3gpu_memory *ctx_reports_mem;
};

TAILQ_HEAD(ps3gpu_context_queue, ps3gpu_context);

#define PS3GPU_CONTEXT_MAX_DISPLAY_BUFFERS	8
#define PS3GPU_CONTEXT_MAX_TILES		16

enum ps3gpu_head {
	PS3GPU_HEAD_A = 0,
	PS3GPU_HEAD_B = 1
};

enum ps3gpu_flip_mode {
	PS3GPU_FLIP_MODE_HSYNC = 1,
	PS3GPU_FLIP_MODE_VSYNC = 2
};

enum ps3gpu_tile_cmp_mode {
	PS3GPU_TILE_CMP_MODE_NONE		= 0,
	PS3GPU_TILE_CMP_MODE_C32_2X1		= 7,
	PS3GPU_TILE_CMP_MODE_C32_2X2		= 8,
	PS3GPU_TILE_CMP_MODE_Z32_SEP		= 9,
	PS3GPU_TILE_CMP_MODE_Z32_SEP_REG	= 10,
	PS3GPU_TILE_CMP_MODE_Z32_SEP_DIAG	= 11,
	PS3GPU_TILE_CMP_MODE_Z32_SEP_ROT	= 12
};

#define PS3GPU_MEMORY_MAP_HANDLE_BITS	24
#define PS3GPU_MEMORY_MAP_HANDLE_SHIFT	(sizeof(unsigned long) * 8 - PS3GPU_MEMORY_MAP_HANDLE_BITS)
#define PS3GPU_MEMORY_MAP_HANDLE(_addr)	((_addr) & ~((1ul << PS3GPU_MEMORY_MAP_HANDLE_SHIFT) - 1))
#define PS3GPU_MEMORY_MAP_OFFSET(_addr)	((_addr) & ((1ul << PS3GPU_MEMORY_MAP_HANDLE_SHIFT) - 1))

struct ps3gpu_memory_map {
	TAILQ_ENTRY(ps3gpu_memory_map) mm_queue;

	struct ps3gpu_memory *mm_mem;

	unsigned long mm_handle;
};

TAILQ_HEAD(ps3gpu_memory_map_queue, ps3gpu_memory_map);

/* Hypervisor limit per LPAR */
#define PS3GPU_DEVICE_MAX_CONTEXTS	3

struct ps3gpu_device {
	struct sx d_lock;

	struct ps3gpu_context *d_context[PS3GPU_DEVICE_MAX_CONTEXTS];

	struct unrhdr *d_mmap_unrhdr;
	struct ps3gpu_memory_map_queue d_mmap_queue;
};

struct ps3gpu_user {
	struct sx u_lock;

	struct ps3gpu_context_queue u_context_queue;
};

static inline unsigned int
ps3gpu_context_get_channel(struct ps3gpu_context *context)
{
	return (*(unsigned int *) (context->ctx_driver_info_vaddr + 0xc));
}

int ps3gpu_context_allocate(int vram_size, struct ps3gpu_context **pcontext);

int ps3gpu_context_free(struct ps3gpu_context *context);

int ps3gpu_memory_allocate(struct ps3gpu_context *context,
    int type, int size, int align, struct ps3gpu_memory **pmem);

int ps3gpu_memory_free(struct ps3gpu_memory *mem);

int ps3gpu_setup_control(struct ps3gpu_context *context,
    unsigned int put, unsigned int get, unsigned int ref);

int ps3gpu_set_flip_mode(struct ps3gpu_context *context, int head, int mode);

int ps3gpu_reset_flip_status(struct ps3gpu_context *context, int head);

int ps3gpu_flip(struct ps3gpu_context *context, int head,
    unsigned int offset);

int ps3gpu_display_buffer_set(struct ps3gpu_context *context,
    int buffer_id, int width, int height, int pitch,
    unsigned int offset);

int ps3gpu_display_buffer_unset(struct ps3gpu_context *context,
    int buffer_id);

int ps3gpu_display_buffer_flip(struct ps3gpu_context *context, int head,
    int buffer_id);

int ps3gpu_tile_get_pitch(int pitch);

int ps3gpu_tile_set(struct ps3gpu_context *context,
    int tile_id, int mem_type, int size, int pitch, int cmp_mode, int bank,
    int base, unsigned int offset);

int ps3gpu_tile_unset(struct ps3gpu_context *context,
    int tile_id, int mem_type, int bank, unsigned int offset);

int ps3gpu_cursor_initialize(struct ps3gpu_context *context, int head);

int ps3gpu_cursor_set_image(struct ps3gpu_context *context, int head,
    unsigned int offset);

int ps3gpu_cursor_set_position(struct ps3gpu_context *context, int head,
    int x, int y);

int ps3gpu_cursor_enable(struct ps3gpu_context *context, int head,
    int enable);

#endif /* _PS3GPU_H */
