/*-
 * Copyright (C) 2011 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include <sys/cdefs.h>
__FBSDID("$FreeBSD$");

#include <sys/param.h>
#include <sys/conf.h>
#include <sys/kernel.h>
#include <sys/malloc.h>

#include "ps3gpu_heap.h"

static MALLOC_DEFINE(M_PS3GPU_HEAP, "ps3gpu_heap", "PS3 GPU memory heap");

int
ps3gpu_heap_init(struct ps3gpu_heap *h, int size)
{
	struct ps3gpu_heap_block *hb;

	h->h_size = size;

	TAILQ_INIT(&h->h_block_queue);

	hb = malloc(sizeof(*hb), M_PS3GPU_HEAP, M_WAITOK | M_ZERO);
	if (!hb)
		return (ENOMEM);

	hb->hb_start = 0;
	hb->hb_size = size;
	hb->hb_free = 1;

	TAILQ_INSERT_TAIL(&h->h_block_queue, hb, hb_queue);

	return (0);
}

void
ps3gpu_heap_fini(struct ps3gpu_heap *h)
{
	struct ps3gpu_heap_block *hb;

	while ((hb = TAILQ_FIRST(&h->h_block_queue))) {
		TAILQ_REMOVE(&h->h_block_queue, hb, hb_queue);
		free(hb, M_PS3GPU_HEAP);
	}
}

int
ps3gpu_heap_allocate(struct ps3gpu_heap *h, int size, int align,
    int *start)
{
	struct ps3gpu_heap_block *hb, *hb_left = NULL, *hb_right = NULL;
	int mask = (1 << align) - 1;
	int new_start;

	TAILQ_FOREACH(hb, &h->h_block_queue, hb_queue) {
		if (!hb->hb_free)
			continue;

		new_start = (hb->hb_start + mask) & ~mask;

		if (new_start + size <= hb->hb_start + hb->hb_size) {
			if (new_start > hb->hb_start) {
				hb_left = malloc(sizeof(*hb_left), M_PS3GPU_HEAP,
				    M_WAITOK | M_ZERO);
				if (!hb_left)
					return (ENOMEM);

				hb_left->hb_start = hb->hb_start;
				hb_left->hb_size = new_start - hb_left->hb_start;
				hb_left->hb_free = 1;
			}

			if (new_start + size < hb->hb_start + hb->hb_size) {
				hb_right = malloc(sizeof(*hb_right), M_PS3GPU_HEAP,
				    M_WAITOK | M_ZERO);
				if (!hb_right) {
					if (hb_left)
						free(hb_left, M_PS3GPU_HEAP);
					return (ENOMEM);
				}

				hb_right->hb_start = new_start + size;
				hb_right->hb_size = hb->hb_start + hb->hb_size -
				    hb_right->hb_start;
				hb_right->hb_free = 1;
			}

			hb->hb_start = new_start;
			hb->hb_size = size;
			hb->hb_free = 0;

			if (hb_left)
				TAILQ_INSERT_BEFORE(hb, hb_left, hb_queue);

			if (hb_right)
				TAILQ_INSERT_AFTER(&h->h_block_queue,
				    hb, hb_right, hb_queue);

			*start = new_start;

			return (0);
		}
	}

	return (ENOMEM);
}

int
ps3gpu_heap_free(struct ps3gpu_heap *h, int start, int size)
{
	struct ps3gpu_heap_block *hb, *hb_left = NULL, *hb_right = NULL;

	TAILQ_FOREACH(hb, &h->h_block_queue, hb_queue) {
		if (start < hb->hb_start)
			break;

		if (hb->hb_free)
			continue;

		if (start >= hb->hb_start &&
		    start + size <= hb->hb_start + hb->hb_size) {
			if (start > hb->hb_start) {
				hb_left = malloc(sizeof(*hb_left), M_PS3GPU_HEAP,
				    M_WAITOK | M_ZERO);
				if (!hb_left)
					return (ENOMEM);

				hb_left->hb_start = hb->hb_start;
				hb_left->hb_size = start - hb_left->hb_start;
				hb_left->hb_free = 0;
			}

			if (start + size < hb->hb_start + hb->hb_size) {
				hb_right = malloc(sizeof(*hb_right), M_PS3GPU_HEAP,
				    M_WAITOK | M_ZERO);
				if (!hb_right) {
					if (hb_left)
						free(hb_left, M_PS3GPU_HEAP);
					return (ENOMEM);
				}

				hb_right->hb_start = start + size;
				hb_right->hb_size = hb->hb_start + hb->hb_size -
				    hb_right->hb_start;
				hb_right->hb_free = 0;
			}

			hb->hb_start = start;
			hb->hb_size = size;
			hb->hb_free = 1;

			if (hb_left)
				TAILQ_INSERT_BEFORE(hb, hb_left, hb_queue);

			if (hb_right)
				TAILQ_INSERT_AFTER(&h->h_block_queue,
				    hb, hb_right, hb_queue);

			return (0);
		}
	}

	return (EINVAL);
}
