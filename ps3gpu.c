/*-
 * Copyright (C) 2011, 2012 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include <sys/cdefs.h>
__FBSDID("$FreeBSD$");

#include <sys/param.h>
#include <sys/bus.h>
#include <sys/conf.h>
#include <sys/kernel.h>
#include <sys/mman.h>
#include <sys/module.h>
#include <sys/systm.h>
#include <sys/fcntl.h>
#include <sys/sx.h>
#include <sys/malloc.h>
#include <sys/ioccom.h>

#include <vm/pmap.h>

#include <machine/pmap.h>

#include <powerpc/ps3/ps3-hvcall.h>

#include "ps3gpu_heap.h"
#include "ps3gpu_ctl.h"
#include "ps3gpu.h"

static int ps3gpu_open(struct cdev *dev, int oflags, int fmt,
    struct thread *td);
static int ps3gpu_mmap(struct cdev *dev, vm_ooffset_t offset,
    vm_paddr_t *paddr, int nprot, vm_memattr_t *memattr);
static int ps3gpu_ioctl(struct cdev *dev, u_long cmd, caddr_t data,
    int fflag, struct thread *td);

static struct ps3gpu_memory_map *ps3gpu_dev_get_mmap(struct ps3gpu_device *dev,
    unsigned long handle);
static unsigned long ps3gpu_dev_alloc_handle(struct ps3gpu_device *dev);
static void ps3gpu_dev_free_handle(struct ps3gpu_device *dev,
    unsigned long handle);

static struct cdevsw ps3gpu_cdevsw = {
	.d_version	= D_VERSION,
	.d_open		= ps3gpu_open,
	.d_mmap		= ps3gpu_mmap,
	.d_ioctl	= ps3gpu_ioctl,
	.d_name		= "ps3gpu",
};

static struct ps3gpu_device *ps3gpu_dev;

static struct cdev *ps3gpu_cdev;

static MALLOC_DEFINE(M_PS3GPU, "ps3gpu", "PS3 GPU");
static MALLOC_DEFINE(M_PS3GPU_GART, "ps3gpu_gart", "PS3 GPU GART memory");

int
ps3gpu_context_allocate(int vram_size, struct ps3gpu_context **pcontext)
{
	struct ps3gpu_context *context;
	int id;
	int err;

	sx_xlock(&ps3gpu_dev->d_lock);

	for (id = 0; id < PS3GPU_DEVICE_MAX_CONTEXTS; id++) {
		if (!ps3gpu_dev->d_context[id])
			break;
	}

	if (id >= PS3GPU_DEVICE_MAX_CONTEXTS) {
		sx_xunlock(&ps3gpu_dev->d_lock);
		return (ENXIO);
	}

	context = malloc(sizeof(*context), M_PS3GPU, M_WAITOK | M_ZERO);
	if (!context) {
		sx_xunlock(&ps3gpu_dev->d_lock);
		return (ENOMEM);
	}

	sx_init(&context->ctx_lock, "ps3gpu_context");

	context->ctx_dev = ps3gpu_dev;
	context->ctx_id = id;
	context->ctx_vram_size = vram_size * 1024 * 1024;

	err = lv1_gpu_memory_allocate(context->ctx_vram_size,
	    0 /* 0x80000 */, 0 /* 0x300000 */, 0 /* 0xf */, 0 /* 0x8 */,
	    &context->ctx_memory_handle, &context->ctx_vram_paddr);
	if (err) {
		printf("lv1_gpu_memory_allocate failed (%d)\n", err);
		err = ENXIO;
		goto free_context;
	}

	context->ctx_vram_vaddr = (unsigned long) pmap_mapdev(context->ctx_vram_paddr,
	    context->ctx_vram_size);
	if (!context->ctx_vram_vaddr) {
		err = ENXIO;
		goto free_vram;
	}

	err = lv1_gpu_context_allocate(context->ctx_memory_handle, 0x822,
	    &context->ctx_handle, &context->ctx_control_paddr,
	    &context->ctx_driver_info_paddr, &context->ctx_reports_paddr,
	    &context->ctx_reports_size);
	if (err) {
		printf("lv1_gpu_context_allocate failed (%d)\n", err);
		err = ENXIO;
		goto unmap_vram;
	}

	context->ctx_control_size = PAGE_SIZE;

	context->ctx_control_vaddr = (unsigned long) pmap_mapdev(context->ctx_control_paddr,
	    context->ctx_control_size);
	if (!context->ctx_control_vaddr) {
		err = ENXIO;
		goto free_gpu_context;
	}

	context->ctx_driver_info_size = 4 * PAGE_SIZE;

	context->ctx_driver_info_vaddr = (unsigned long) pmap_mapdev(context->ctx_driver_info_paddr,
	    context->ctx_driver_info_size);
	if (!context->ctx_driver_info_vaddr) {
		err = ENXIO;
		goto unmap_control;
	}

	context->ctx_reports_vaddr = (unsigned long) pmap_mapdev(context->ctx_reports_paddr,
	    context->ctx_reports_size);
	if (!context->ctx_reports_vaddr) {
		err = ENXIO;
		goto unmap_driver_info;
	}

	err = ps3gpu_heap_init(&context->ctx_heap[PS3GPU_MEMORY_TYPE_VIDEO],
	    context->ctx_vram_size);
	if (err)
		goto unmap_reports;

	err = ps3gpu_heap_init(&context->ctx_heap[PS3GPU_MEMORY_TYPE_GART],
	    256 * 1024 * 1024);
	if (err)
		goto fini_heap_vram;

	TAILQ_INIT(&context->ctx_mem_queue);

	ps3gpu_dev->d_context[id] = context;

	sx_xunlock(&ps3gpu_dev->d_lock);

	*pcontext = context;

	return (0);

fini_heap_vram:

	ps3gpu_heap_fini(&context->ctx_heap[PS3GPU_MEMORY_TYPE_VIDEO]);

unmap_reports:

	pmap_unmapdev(context->ctx_reports_vaddr, context->ctx_reports_size);

unmap_driver_info:

	pmap_unmapdev(context->ctx_driver_info_vaddr, context->ctx_driver_info_size);

unmap_control:

	pmap_unmapdev(context->ctx_control_vaddr, context->ctx_control_size);

free_gpu_context:

	lv1_gpu_context_free(context->ctx_handle);

unmap_vram:

	pmap_unmapdev(context->ctx_vram_vaddr,
	    context->ctx_vram_size);

free_vram:

	lv1_gpu_memory_free(context->ctx_memory_handle);

free_context:

	sx_destroy(&context->ctx_lock);

	free(context, M_PS3GPU);

	sx_xunlock(&ps3gpu_dev->d_lock);

	return (err);
}

int
ps3gpu_context_free(struct ps3gpu_context *context)
{
	struct ps3gpu_memory *mem;

	KASSERT(context != NULL, ("invalid context"));

	sx_xlock(&context->ctx_dev->d_lock);
	ps3gpu_dev->d_context[context->ctx_id] = NULL;
	sx_xunlock(&context->ctx_dev->d_lock);

	while ((mem = TAILQ_FIRST(&context->ctx_mem_queue)))
		ps3gpu_memory_free(mem);

	pmap_unmapdev(context->ctx_vram_vaddr, context->ctx_vram_size);
	pmap_unmapdev(context->ctx_control_vaddr, context->ctx_control_size);
	pmap_unmapdev(context->ctx_driver_info_vaddr,
	    context->ctx_driver_info_size);
	pmap_unmapdev(context->ctx_reports_vaddr, context->ctx_reports_size);

	ps3gpu_heap_fini(&context->ctx_heap[PS3GPU_MEMORY_TYPE_VIDEO]);
	ps3gpu_heap_fini(&context->ctx_heap[PS3GPU_MEMORY_TYPE_GART]);

	lv1_gpu_context_free(context->ctx_handle);

	lv1_gpu_memory_free(context->ctx_memory_handle);

	sx_destroy(&context->ctx_lock);

	free(context, M_PS3GPU);

	return (0);
}

int
ps3gpu_memory_allocate(struct ps3gpu_context *context,
    int type, int size, int align, struct ps3gpu_memory **pmem)
{
	struct ps3gpu_memory *mem;
	int start;
	int i;
	int err;

	KASSERT(context != NULL, ("invalid context"));
	KASSERT(type == PS3GPU_MEMORY_TYPE_VIDEO ||
	    type == PS3GPU_MEMORY_TYPE_GART, ("invalid memory type"));
	KASSERT((size % PAGE_SIZE) == 0,
	    ("size is not a multiple of page size"));
	KASSERT(align >= PAGE_SHIFT, ("alignment is too small"));

	sx_xlock(&context->ctx_lock);

	err = ps3gpu_heap_allocate(&context->ctx_heap[type],
	    size, align, &start);
	if (err) {
		sx_xunlock(&context->ctx_lock);
		return (err);
	}

	mem = malloc(sizeof(*mem), M_PS3GPU, M_WAITOK | M_ZERO);
	if (!mem) {
		err = ENOMEM;
		goto free_heap;
	}

	mem->m_context = context;

	mem->m_type = type;
	mem->m_start = start;
	mem->m_size = size;

	if (type == PS3GPU_MEMORY_TYPE_VIDEO) {
		mem->m_paddr = context->ctx_vram_paddr + mem->m_start;
		mem->m_vaddr = context->ctx_vram_vaddr + mem->m_start;
		mem->m_gaddr = mem->m_start;
	} else {
		mem->m_vaddr = (unsigned long) malloc(size, M_PS3GPU_GART,
		    M_WAITOK | M_ZERO);
		if (!mem->m_vaddr) {
			err = ENOMEM;
			goto free_memory;
		}

		mem->m_gaddr = PS3GPU_MEMORY_GART_ADDRESS_OFFSET +
		    mem->m_start;

		for (i = 0; i < size / PAGE_SIZE; i++) {
			err = lv1_gpu_context_iomap(context->ctx_handle,
			    mem->m_gaddr + i * PAGE_SIZE,
			    vtophys(mem->m_vaddr + i * PAGE_SIZE),
			    PAGE_SIZE, 0xe000000000000800ul);
			if (err) {
				printf("lv1_gpu_context_iomap failed (%d)\n", err);
				err = ENXIO;
				goto free_gart;
			}
		}
	}

	TAILQ_INSERT_TAIL(&context->ctx_mem_queue, mem, m_queue);

	sx_xunlock(&context->ctx_lock);

	*pmem = mem;

	return (0);

free_gart:

	for (i = 0; i < size / PAGE_SIZE; i++) {
		lv1_gpu_context_iomap(context->ctx_handle,
		    mem->m_gaddr + i * PAGE_SIZE,
		    vtophys(mem->m_vaddr + i * PAGE_SIZE),
		    PAGE_SIZE, 0x2000000000000000ul);
	}

	free((void *) mem->m_vaddr, M_PS3GPU_GART);

free_memory:

	free(mem, M_PS3GPU);

free_heap:

	ps3gpu_heap_free(&context->ctx_heap[type], start, size);

	sx_xunlock(&context->ctx_lock);

	return (err);
}

int
ps3gpu_memory_free(struct ps3gpu_memory *mem)
{
	struct ps3gpu_context *context = mem->m_context;
	int i;

	KASSERT(mem != NULL, ("invalid memory"));
	KASSERT(mem->m_type == PS3GPU_MEMORY_TYPE_VIDEO ||
	    mem->m_type == PS3GPU_MEMORY_TYPE_GART, ("invalid memory type"));
	KASSERT(context != NULL, ("invalid context"));

	sx_xlock(&context->ctx_lock);

	TAILQ_REMOVE(&context->ctx_mem_queue, mem, m_queue);

	if (mem->m_type == PS3GPU_MEMORY_TYPE_GART) {
		for (i = 0; i < mem->m_size / PAGE_SIZE; i++) {
			lv1_gpu_context_iomap(context->ctx_handle,
			    mem->m_gaddr + i * PAGE_SIZE,
			    vtophys(mem->m_vaddr + i * PAGE_SIZE),
			    PAGE_SIZE, 0x2000000000000000ul);
		}

		free((void *) mem->m_vaddr, M_PS3GPU_GART);
	}

	ps3gpu_heap_free(&context->ctx_heap[mem->m_type],
	    mem->m_start, mem->m_size);

	free(mem, M_PS3GPU);

	sx_xunlock(&context->ctx_lock);

	return (0);
}

int
ps3gpu_setup_control(struct ps3gpu_context *context,
    unsigned int put, unsigned int get, unsigned int ref)
{
	int err;

	KASSERT(context != NULL, ("invalid context"));

	sx_xlock(&context->ctx_lock);

	err = lv1_gpu_context_attribute(context->ctx_handle, 0x1,
	    put, get, ref, 0);
	if (err) {
		printf("lv1_gpu_context_attribute failed (%d)\n", err);
		sx_xunlock(&context->ctx_lock);
		return (ENXIO);
	}

	sx_xunlock(&context->ctx_lock);

	return (0);
}

int
ps3gpu_set_flip_mode(struct ps3gpu_context *context, int head, int mode)
{
	int err;

	KASSERT(context != NULL, ("invalid context"));
	KASSERT(head == PS3GPU_HEAD_A || head == PS3GPU_HEAD_B,
	    ("invalid head"));
	KASSERT(mode == PS3GPU_FLIP_MODE_HSYNC ||
	    PS3GPU_FLIP_MODE_VSYNC, ("invalid flip mode"));

	sx_xlock(&context->ctx_lock);

	err = lv1_gpu_attribute(0x2, head, 0, 0, 0);
	if (err) {
		printf("lv1_gpu_attribute failed (%d)\n", err);
		sx_xunlock(&context->ctx_lock);
		return (ENXIO);
	}

	err = lv1_gpu_context_attribute(context->ctx_handle, 0x101,
	    head, mode, 0, 0);
	if (err) {
		printf("lv1_gpu_context_attribute failed (%d)\n", err);
		sx_xunlock(&context->ctx_lock);
		return (ENXIO);
	}

	sx_xunlock(&context->ctx_lock);

	return (0);
}

int
ps3gpu_reset_flip_status(struct ps3gpu_context *context, int head)
{
	int err;

	KASSERT(context != NULL, ("invalid context"));
	KASSERT(head == PS3GPU_HEAD_A || head == PS3GPU_HEAD_B,
	    ("invalid head"));

	sx_xlock(&context->ctx_lock);

	err = lv1_gpu_context_attribute(context->ctx_handle, 0x10a,
	    head, 0x7ffffffful, 0x00000000ul, 0);
	if (err) {
		printf("lv1_gpu_context_attribute failed (%d)\n", err);
		sx_xunlock(&context->ctx_lock);
		return (ENXIO);
	}

	sx_xunlock(&context->ctx_lock);

	return (0);
}

int
ps3gpu_flip(struct ps3gpu_context *context, int head,
    unsigned int offset)
{
	int err;

	KASSERT(context != NULL, ("invalid context"));
	KASSERT(head == PS3GPU_HEAD_A || head == PS3GPU_HEAD_B,
	    ("invalid head"));

	sx_xlock(&context->ctx_lock);

	err = lv1_gpu_context_attribute(context->ctx_handle, 0x102,
	    head, offset, 0, 0);
	if (err) {
		printf("lv1_gpu_context_attribute failed (%d)\n", err);
		sx_xunlock(&context->ctx_lock);
		return (ENXIO);
	}

	sx_xunlock(&context->ctx_lock);

	return (0);
}

int
ps3gpu_display_buffer_set(struct ps3gpu_context *context,
    int buffer_id, int width, int height, int pitch,
    unsigned int offset)
{
	int err;

	KASSERT(context != NULL, ("invalid context"));
	KASSERT(buffer_id >= 0 &&
	    buffer_id < PS3GPU_CONTEXT_MAX_DISPLAY_BUFFERS,
	    ("invalid display buffer id"));

	sx_xlock(&context->ctx_lock);

	err = lv1_gpu_context_attribute(context->ctx_handle, 0x104,
	    buffer_id, (((unsigned long) width) << 32) | height,
	    (((unsigned long) pitch) << 32) | offset, 0);
	if (err) {
		printf("lv1_gpu_context_attribute failed (%d)\n", err);
		sx_xunlock(&context->ctx_lock);
		return (ENXIO);
	}

	sx_xunlock(&context->ctx_lock);

	return (0);
}

int
ps3gpu_display_buffer_unset(struct ps3gpu_context *context,
   int buffer_id)
{
	int err;

	KASSERT(context != NULL, ("invalid context"));
	KASSERT(buffer_id >= 0 &&
	    buffer_id < PS3GPU_CONTEXT_MAX_DISPLAY_BUFFERS,
	    ("invalid display buffer id"));

	sx_xlock(&context->ctx_lock);

	err = lv1_gpu_context_attribute(context->ctx_handle, 0x105,
	    buffer_id, 0, 0, 0);
	if (err) {
		printf("lv1_gpu_context_attribute failed (%d)\n", err);
		sx_xunlock(&context->ctx_lock);
		return (ENXIO);
	}

	sx_xunlock(&context->ctx_lock);

	return (0);
}

int
ps3gpu_display_buffer_flip(struct ps3gpu_context *context, int head,
    int buffer_id)
{
	int err;

	KASSERT(context != NULL, ("invalid context"));
	KASSERT(head == PS3GPU_HEAD_A || head == PS3GPU_HEAD_B,
	    ("invalid head"));
	KASSERT(buffer_id >= 0 &&
	    buffer_id < PS3GPU_CONTEXT_MAX_DISPLAY_BUFFERS,
	    ("invalid display buffer id"));

	sx_xlock(&context->ctx_lock);

	err = lv1_gpu_context_attribute(context->ctx_handle, 0x102,
	    head, 0x80000000ul | (ps3gpu_context_get_channel(context) << 8) |
	        buffer_id, 0, 0);
	if (err) {
		printf("lv1_gpu_context_attribute failed (%d)\n", err);
		sx_xunlock(&context->ctx_lock);
		return (ENXIO);
	}

	sx_xunlock(&context->ctx_lock);

	return (0);
}

int
ps3gpu_tile_get_pitch(int pitch)
{
	static const int tile_pitches[] = {
		0x00000000, 0x00000200, 0x00000300, 0x00000400,
		0x00000500, 0x00000600, 0x00000700, 0x00000800,
		0x00000a00, 0x00000c00, 0x00000d00, 0x00000e00,
		0x00001000, 0x00001400, 0x00001800, 0x00001a00,
		0x00001c00, 0x00002000, 0x00002800, 0x00003000,
		0x00003400, 0x00003800, 0x00004000, 0x00005000,
		0x00006000, 0x00006800, 0x00007000, 0x00008000,
		0x0000a000, 0x0000c000, 0x0000d000, 0x0000e000,
		0x00010000,

	};
	int i;

	for (i = 0; i < sizeof(tile_pitches) / sizeof(tile_pitches[0]) - 1; i++) {
		if ((tile_pitches[i] < pitch) && (pitch <= tile_pitches[i + 1]))
			return (tile_pitches[i + 1]);
	}

	return (0);
}

int
ps3gpu_tile_set(struct ps3gpu_context *context,
    int tile_id, int mem_type, int size, int pitch, int cmp_mode, int bank,
    int base, unsigned int offset)
{
	int location;
	unsigned long p1, p2, p3, p4;
	int err;

	KASSERT(context != NULL, ("invalid context"));
	KASSERT(tile_id >= 0 &&
	    tile_id < PS3GPU_CONTEXT_MAX_TILES,
	    ("invalid tile id"));
	KASSERT(mem_type == PS3GPU_MEMORY_TYPE_VIDEO ||
	    mem_type == PS3GPU_MEMORY_TYPE_GART, ("invalid memory type"));

	location = (mem_type == PS3GPU_MEMORY_TYPE_GART) ? 1 : 0;

	p1 = (location << 31) | (offset & ~0xfffful) | (bank << 4) | (location + 1);
	p2 = (location << 31) | ((offset + size - 1) & ~0xfffful);
	p3 = pitch & ~0xfffful;
	p4 = 0x40000000ul | (cmp_mode << 26) | ((base + ((size - 1) >> 16)) << 13) | base;
	
	sx_xlock(&context->ctx_lock);

	err = lv1_gpu_context_attribute(context->ctx_handle, 0x300,
	    tile_id, (p1 << 32) | p2, (p3 << 32) | p4, 0);
	if (err) {
		printf("lv1_gpu_context_attribute failed (%d)\n", err);
		sx_xunlock(&context->ctx_lock);
		return (ENXIO);
	}

	sx_xunlock(&context->ctx_lock);

	return (0);
}

int
ps3gpu_tile_unset(struct ps3gpu_context *context,
    int tile_id, int mem_type, int bank, unsigned int offset)
{
	int location;
	unsigned long p1;
	int err;

	KASSERT(context != NULL, ("invalid context"));
	KASSERT(tile_id >= 0 &&
	    tile_id < PS3GPU_CONTEXT_MAX_TILES,
	    ("invalid tile id"));
	KASSERT(mem_type == PS3GPU_MEMORY_TYPE_VIDEO ||
	    mem_type == PS3GPU_MEMORY_TYPE_GART, ("invalid memory type"));

	location = (mem_type == PS3GPU_MEMORY_TYPE_GART) ? 1 : 0;

	p1 = (location << 31) | (offset & ~0xfffful) | (bank << 4);

	sx_xlock(&context->ctx_lock);

	err = lv1_gpu_context_attribute(context->ctx_handle, 0x300,
	    tile_id, p1 << 32, 0, 0);
	if (err) {
		printf("lv1_gpu_context_attribute failed (%d)\n", err);
		sx_xunlock(&context->ctx_lock);
		return (ENXIO);
	}

	sx_xunlock(&context->ctx_lock);

	return (0);
}

int
ps3gpu_cursor_initialize(struct ps3gpu_context *context, int head)
{
	int err;

	KASSERT(context != NULL, ("invalid context"));
	KASSERT(head == PS3GPU_HEAD_A || head == PS3GPU_HEAD_B,
	    ("invalid head"));

	sx_xlock(&context->ctx_lock);

	err = lv1_gpu_context_attribute(context->ctx_handle, 0x10b,
	    head, 0x1, 0, 0);
	if (err) {
		printf("lv1_gpu_context_attribute failed (%d)\n", err);
		sx_xunlock(&context->ctx_lock);
		return (ENXIO);
	}

	sx_xunlock(&context->ctx_lock);

	return (0);
}

int
ps3gpu_cursor_set_image(struct ps3gpu_context *context, int head,
    unsigned int offset)
{
	int err;

	KASSERT(context != NULL, ("invalid context"));
	KASSERT(head == PS3GPU_HEAD_A || head == PS3GPU_HEAD_B,
	    ("invalid head"));

	sx_xlock(&context->ctx_lock);

	err = lv1_gpu_context_attribute(context->ctx_handle, 0x10b,
	    head, 0x2, offset, 0);
	if (err) {
		printf("lv1_gpu_context_attribute failed (%d)\n", err);
		sx_xunlock(&context->ctx_lock);
		return (ENXIO);
	}

	sx_xunlock(&context->ctx_lock);

	return (0);
}

int
ps3gpu_cursor_set_position(struct ps3gpu_context *context, int head,
    int x, int y)
{
	int err;

	KASSERT(context != NULL, ("invalid context"));
	KASSERT(head == PS3GPU_HEAD_A || head == PS3GPU_HEAD_B,
	    ("invalid head"));

	sx_xlock(&context->ctx_lock);

	err = lv1_gpu_context_attribute(context->ctx_handle, 0x10b,
	    head, 0x3, x, y);
	if (err) {
		printf("lv1_gpu_context_attribute failed (%d)\n", err);
		sx_xunlock(&context->ctx_lock);
		return (ENXIO);
	}

	sx_xunlock(&context->ctx_lock);

	return (0);
}

int
ps3gpu_cursor_enable(struct ps3gpu_context *context, int head,
    int enable)
{
	int err;

	KASSERT(context != NULL, ("invalid context"));
	KASSERT(head == PS3GPU_HEAD_A || head == PS3GPU_HEAD_B,
	    ("invalid head"));

	sx_xlock(&context->ctx_lock);

	err = lv1_gpu_context_attribute(context->ctx_handle, 0x10c,
	    head, enable ? 0x1 : 0x2, 0, 0);
	if (err) {
		printf("lv1_gpu_context_attribute failed (%d)\n", err);
		sx_xunlock(&context->ctx_lock);
		return (ENXIO);
	}

	sx_xunlock(&context->ctx_lock);

	return (0);
}

static struct ps3gpu_context *
ps3gpu_user_get_context(struct ps3gpu_user *user, int id)
{
	struct ps3gpu_context *context;

	KASSERT(user != NULL, ("invalid user"));

	sx_xlock(&user->u_lock);

	TAILQ_FOREACH(context, &user->u_context_queue, ctx_user_queue) {
		if (context->ctx_id == id) {
			sx_xunlock(&user->u_lock);
			return (context);
		}
	}

	sx_xunlock(&user->u_lock);

	return (NULL);
}

static int
ps3gpu_ctl_context_allocate(struct ps3gpu_user *user,
    struct ps3gpu_ctl_context_allocate *req)
{
	struct ps3gpu_context *context;
	struct ps3gpu_memory *mem;
	struct ps3gpu_memory_map *control_mmap;
	struct ps3gpu_memory_map *driver_info_mmap;
	struct ps3gpu_memory_map *reports_mmap;
	int err;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	err = ps3gpu_context_allocate(req->vram_size, &context);
	if (err)
		return (err);

	context->ctx_user = user;

	mem = malloc(sizeof(*mem), M_PS3GPU, M_WAITOK | M_ZERO);
	if (!mem) {
		err = ENOMEM;
		goto free_context;
	}

	mem->m_context = context;

	mem->m_type = PS3GPU_MEMORY_TYPE_REGISTER;
	mem->m_size = context->ctx_control_size;

	mem->m_paddr = context->ctx_control_paddr;
	mem->m_vaddr = context->ctx_control_vaddr;

	context->ctx_control_mem = mem;

	control_mmap = malloc(sizeof(*control_mmap), M_PS3GPU,
	    M_WAITOK | M_ZERO);
	if (!control_mmap) {
		err = ENOMEM;
		goto free_control_memory;
	}

	control_mmap->mm_mem = mem;
	control_mmap->mm_handle = ps3gpu_dev_alloc_handle(context->ctx_dev);

	mem = malloc(sizeof(*mem), M_PS3GPU, M_WAITOK | M_ZERO);
	if (!mem) {
		err = ENOMEM;
		goto free_control_memory_map;
	}

	mem->m_context = context;

	mem->m_type = PS3GPU_MEMORY_TYPE_REGISTER;
	mem->m_size = context->ctx_driver_info_size;

	mem->m_paddr = context->ctx_driver_info_paddr;
	mem->m_vaddr = context->ctx_driver_info_vaddr;

	context->ctx_driver_info_mem = mem;

	driver_info_mmap = malloc(sizeof(*driver_info_mmap), M_PS3GPU,
	    M_WAITOK | M_ZERO);
	if (!driver_info_mmap) {
		err = ENOMEM;
		goto free_driver_info_memory;
	}

	driver_info_mmap->mm_mem = mem;
	driver_info_mmap->mm_handle = ps3gpu_dev_alloc_handle(context->ctx_dev);

	mem = malloc(sizeof(*mem), M_PS3GPU, M_WAITOK | M_ZERO);
	if (!mem) {
		err = ENOMEM;
		goto free_driver_info_memory_map;
	}

	mem->m_context = context;

	mem->m_type = PS3GPU_MEMORY_TYPE_REGISTER;
	mem->m_size = context->ctx_reports_size;

	mem->m_paddr = context->ctx_reports_paddr;
	mem->m_vaddr = context->ctx_reports_vaddr;

	context->ctx_reports_mem = mem;

	reports_mmap = malloc(sizeof(*reports_mmap), M_PS3GPU,
	    M_WAITOK | M_ZERO);
	if (!reports_mmap) {
		err = ENOMEM;
		goto free_reports_memory;
	}

	reports_mmap->mm_mem = mem;
	reports_mmap->mm_handle = ps3gpu_dev_alloc_handle(context->ctx_dev);

	sx_xlock(&context->ctx_dev->d_lock);
	TAILQ_INSERT_TAIL(&context->ctx_dev->d_mmap_queue, control_mmap, mm_queue);
	TAILQ_INSERT_TAIL(&context->ctx_dev->d_mmap_queue, driver_info_mmap, mm_queue);
	TAILQ_INSERT_TAIL(&context->ctx_dev->d_mmap_queue, reports_mmap, mm_queue);
	sx_xunlock(&context->ctx_dev->d_lock);

	sx_xlock(&user->u_lock);
	TAILQ_INSERT_TAIL(&user->u_context_queue, context, ctx_user_queue);
	sx_xunlock(&user->u_lock);

	req->context_id = context->ctx_id;
	req->control_handle = control_mmap->mm_handle;
	req->control_size = control_mmap->mm_mem->m_size;
	req->driver_info_handle = driver_info_mmap->mm_handle;
	req->driver_info_size = driver_info_mmap->mm_mem->m_size;
	req->reports_handle = reports_mmap->mm_handle;
	req->reports_size = reports_mmap->mm_mem->m_size;

	return (0);

free_reports_memory:

	free(context->ctx_reports_mem, M_PS3GPU);

free_driver_info_memory_map:

	ps3gpu_dev_free_handle(context->ctx_dev, driver_info_mmap->mm_handle);
	free(driver_info_mmap, M_PS3GPU);

free_driver_info_memory:

	free(context->ctx_driver_info_mem, M_PS3GPU);

free_control_memory_map:

	ps3gpu_dev_free_handle(context->ctx_dev, control_mmap->mm_handle);
	free(control_mmap, M_PS3GPU);

free_control_memory:

	free(context->ctx_control_mem, M_PS3GPU);

free_context:

	ps3gpu_context_free(context);

	return (err);
}

static int
ps3gpu_ctl_context_free(struct ps3gpu_user *user,
    struct ps3gpu_ctl_context_free *req)
{
	struct ps3gpu_context *context;
	struct ps3gpu_memory_map *mmap, *mmap_temp;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	context = ps3gpu_user_get_context(user, req->context_id);
	if (!context)
		return (EINVAL);

	sx_xlock(&user->u_lock);
	TAILQ_REMOVE(&user->u_context_queue, context, ctx_user_queue);
	sx_xunlock(&user->u_lock);

	sx_xlock(&context->ctx_dev->d_lock);

	TAILQ_FOREACH_SAFE(mmap, &context->ctx_dev->d_mmap_queue, mm_queue, mmap_temp) {
		if (mmap->mm_mem->m_context == context) {
			TAILQ_REMOVE(&context->ctx_dev->d_mmap_queue, mmap, mm_queue);
			free_unr(context->ctx_dev->d_mmap_unrhdr,
			    mmap->mm_handle >> PS3GPU_MEMORY_MAP_HANDLE_SHIFT);
			free(mmap, M_PS3GPU);
		}
	}

	sx_xunlock(&context->ctx_dev->d_lock);

	free(context->ctx_control_mem, M_PS3GPU);
	free(context->ctx_driver_info_mem, M_PS3GPU);
	free(context->ctx_reports_mem, M_PS3GPU);

	ps3gpu_context_free(context);

	return (0);
}

static int
ps3gpu_ctl_memory_allocate(struct ps3gpu_user *user,
    struct ps3gpu_ctl_memory_allocate *req)
{
	struct ps3gpu_context *context;
	struct ps3gpu_memory *mem;
	struct ps3gpu_memory_map *mmap;
	int type;
	int err;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	switch (req->type) {
	case PS3GPU_CTL_MEMORY_TYPE_VIDEO:
		type = PS3GPU_MEMORY_TYPE_VIDEO;
	break;
	case PS3GPU_CTL_MEMORY_TYPE_GART:
		type = PS3GPU_MEMORY_TYPE_GART;
	break;
	default:
		return (EINVAL);
	}

	if (req->size % PAGE_SIZE)
		return (EINVAL);

	if (req->align < PAGE_SHIFT)
		return (EINVAL);

	context = ps3gpu_user_get_context(user, req->context_id);
	if (!context)
		return (EINVAL);

	err = ps3gpu_memory_allocate(context, type, req->size, req->align,
	    &mem);
	if (err)
		return (err);

	mmap = malloc(sizeof(*mmap), M_PS3GPU, M_WAITOK | M_ZERO);
	if (err) {
		ps3gpu_memory_free(mem);
		return (ENOMEM);
	}

	mmap->mm_mem = mem;
	mmap->mm_handle = ps3gpu_dev_alloc_handle(context->ctx_dev);

	sx_xlock(&context->ctx_dev->d_lock);
	TAILQ_INSERT_TAIL(&context->ctx_dev->d_mmap_queue, mmap, mm_queue);
	sx_xunlock(&context->ctx_dev->d_lock);

	req->handle = mmap->mm_handle;
	req->gpu_addr = mmap->mm_mem->m_gaddr;

	return (0);
}

static int
ps3gpu_ctl_memory_free(struct ps3gpu_user *user,
    struct ps3gpu_ctl_memory_free *req)
{
	struct ps3gpu_context *context;
	struct ps3gpu_memory *mem;
	struct ps3gpu_memory_map *mmap;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	mmap = ps3gpu_dev_get_mmap(ps3gpu_dev, req->handle);
	if (!mmap)
		return (EINVAL);

	mem = mmap->mm_mem;
	context = mem->m_context;

	KASSERT(context != NULL, ("invalid context"));

	sx_xlock(&context->ctx_dev->d_lock);
	TAILQ_REMOVE(&context->ctx_dev->d_mmap_queue, mmap, mm_queue);
	sx_xunlock(&context->ctx_dev->d_lock);

	ps3gpu_dev_free_handle(context->ctx_dev, mmap->mm_handle);

	free(mmap, M_PS3GPU);

	ps3gpu_memory_free(mem);

	return (0);
}

static int
ps3gpu_ctl_setup_control(struct ps3gpu_user *user,
    struct ps3gpu_ctl_setup_control *req)
{
	struct ps3gpu_context *put_context, *get_context;
	struct ps3gpu_memory *put_mem, *get_mem;
	struct ps3gpu_memory_map *put_mmap, *get_mmap;
	unsigned long put_handle, put_offset, get_handle, get_offset;
	int err;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	put_handle = PS3GPU_MEMORY_MAP_HANDLE(req->put);
	put_offset = PS3GPU_MEMORY_MAP_OFFSET(req->put);

	put_mmap = ps3gpu_dev_get_mmap(ps3gpu_dev, put_handle);
	if (!put_mmap)
		return (EINVAL);

	put_mem = put_mmap->mm_mem;
	put_context = put_mem->m_context;

	KASSERT(put_context != NULL, ("invalid context"));

	get_handle = PS3GPU_MEMORY_MAP_HANDLE(req->get);
	get_offset = PS3GPU_MEMORY_MAP_OFFSET(req->get);

	get_mmap = ps3gpu_dev_get_mmap(ps3gpu_dev, get_handle);
	if (!get_mmap)
		return (EINVAL);

	get_mem = get_mmap->mm_mem;
	get_context = get_mem->m_context;

	KASSERT(get_context != NULL, ("invalid context"));

	if (put_context->ctx_id != req->context_id ||
	    put_context != get_context)
		return (EINVAL);

	err = ps3gpu_setup_control(put_context, put_mem->m_gaddr + put_offset,
	    get_mem->m_gaddr + get_offset, req->ref);
	if (err)
		return (err);

	return (0);
}

static int
ps3gpu_ctl_set_flip_mode(struct ps3gpu_user *user,
    struct ps3gpu_ctl_set_flip_mode *req)
{
	struct ps3gpu_context *context;
	int head, mode;
	int err;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	switch (req->head) {
	case PS3GPU_CTL_HEAD_A:
		head = PS3GPU_HEAD_A;
	break;
	case PS3GPU_CTL_HEAD_B:
		head = PS3GPU_HEAD_B;
	break;
	default:
		return (EINVAL);
	}

	switch (req->mode) {
	case PS3GPU_CTL_FLIP_MODE_HSYNC:
		mode = PS3GPU_FLIP_MODE_HSYNC;
	break;
	case PS3GPU_CTL_FLIP_MODE_VSYNC:
		mode = PS3GPU_FLIP_MODE_VSYNC;
	break;
	default:
		return (EINVAL);
	}

	context = ps3gpu_user_get_context(user, req->context_id);
	if (!context)
		return (EINVAL);

	err = ps3gpu_set_flip_mode(context, head, mode);
	if (err)
		return (err);

	return (0);
}

static int
ps3gpu_ctl_reset_flip_status(struct ps3gpu_user *user,
    struct ps3gpu_ctl_reset_flip_status *req)
{
	struct ps3gpu_context *context;
	int head;
	int err;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	switch (req->head) {
	case PS3GPU_CTL_HEAD_A:
		head = PS3GPU_HEAD_A;
	break;
	case PS3GPU_CTL_HEAD_B:
		head = PS3GPU_HEAD_B;
	break;
	default:
		return (EINVAL);
	}

	context = ps3gpu_user_get_context(user, req->context_id);
	if (!context)
		return (EINVAL);

	err = ps3gpu_reset_flip_status(context, head);
	if (err)
		return (err);

	return (0);
}

static int
ps3gpu_ctl_flip(struct ps3gpu_user *user,
    struct ps3gpu_ctl_flip *req)
{
	struct ps3gpu_context *context;
	struct ps3gpu_memory *mem;
	struct ps3gpu_memory_map *mmap;
	int head;
	unsigned long handle, offset;
	int err;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	switch (req->head) {
	case PS3GPU_CTL_HEAD_A:
		head = PS3GPU_HEAD_A;
	break;
	case PS3GPU_CTL_HEAD_B:
		head = PS3GPU_HEAD_B;
	break;
	default:
		return (EINVAL);
	}

	handle = PS3GPU_MEMORY_MAP_HANDLE(req->offset);
	offset = PS3GPU_MEMORY_MAP_OFFSET(req->offset);

	mmap = ps3gpu_dev_get_mmap(ps3gpu_dev, handle);
	if (!mmap)
		return (EINVAL);

	mem = mmap->mm_mem;
	context = mem->m_context;

	KASSERT(context != NULL, ("invalid context"));

	if (req->context_id != context->ctx_id)
		return (EINVAL);

	err = ps3gpu_flip(context, head, mem->m_start + offset);
	if (err)
		return (err);

	return (0);
}

static int
ps3gpu_ctl_display_buffer_set(struct ps3gpu_user *user,
    struct ps3gpu_ctl_display_buffer_set *req)
{
	struct ps3gpu_context *context;
	struct ps3gpu_memory *mem;
	struct ps3gpu_memory_map *mmap;
	unsigned long handle, offset;
	int err;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	if (req->buffer_id < 0 ||
	    req->buffer_id >= PS3GPU_CONTEXT_MAX_DISPLAY_BUFFERS)
		return (EINVAL);

	handle = PS3GPU_MEMORY_MAP_HANDLE(req->offset);
	offset = PS3GPU_MEMORY_MAP_OFFSET(req->offset);

	mmap = ps3gpu_dev_get_mmap(ps3gpu_dev, handle);
	if (!mmap)
		return (EINVAL);

	mem = mmap->mm_mem;
	context = mem->m_context;

	KASSERT(context != NULL, ("invalid context"));

	if (req->context_id != context->ctx_id)
		return (EINVAL);

	err = ps3gpu_display_buffer_set(context, req->buffer_id,
	    req->width, req->height, req->pitch, mem->m_gaddr + offset);
	if (err)
		return (err);

	return (0);
}

static int
ps3gpu_ctl_display_buffer_unset(struct ps3gpu_user *user,
    struct ps3gpu_ctl_display_buffer_unset *req)
{
	struct ps3gpu_context *context;
	int err;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	if (req->buffer_id < 0 ||
	    req->buffer_id >= PS3GPU_CONTEXT_MAX_DISPLAY_BUFFERS)
		return (EINVAL);

	context = ps3gpu_user_get_context(user, req->context_id);
	if (!context)
		return (EINVAL);

	err = ps3gpu_display_buffer_unset(context, req->buffer_id);
	if (err)
		return (err);

	return (0);
}

static int
ps3gpu_ctl_display_buffer_flip(struct ps3gpu_user *user,
    struct ps3gpu_ctl_display_buffer_flip *req)
{
	struct ps3gpu_context *context;
	int head;
	int err;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	switch (req->head) {
	case PS3GPU_CTL_HEAD_A:
		head = PS3GPU_HEAD_A;
	break;
	case PS3GPU_CTL_HEAD_B:
		head = PS3GPU_HEAD_B;
	break;
	default:
		return (EINVAL);
	}

	if (req->buffer_id < 0 ||
	    req->buffer_id >= PS3GPU_CONTEXT_MAX_DISPLAY_BUFFERS)
		return (EINVAL);

	context = ps3gpu_user_get_context(user, req->context_id);
	if (!context)
		return (EINVAL);

	if (req->context_id != context->ctx_id)
		return (EINVAL);

	err = ps3gpu_display_buffer_flip(context, head, req->buffer_id);
	if (err)
		return (err);
	
	return (0);
}

static int
ps3gpu_ctl_tile_set(struct ps3gpu_user *user,
    struct ps3gpu_ctl_tile_set *req)
{
	struct ps3gpu_context *context;
	struct ps3gpu_memory *mem;
	struct ps3gpu_memory_map *mmap;
	unsigned long handle, offset;
	int cmp_mode;
	int err;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	switch (req->cmp_mode) {
	case PS3GPU_CTL_TILE_CMP_MODE_NONE:
		cmp_mode = PS3GPU_TILE_CMP_MODE_NONE;
	break;
	case PS3GPU_CTL_TILE_CMP_MODE_C32_2X1:
		cmp_mode = PS3GPU_TILE_CMP_MODE_C32_2X1;
	break;
	case PS3GPU_CTL_TILE_CMP_MODE_C32_2X2:
		cmp_mode = PS3GPU_TILE_CMP_MODE_C32_2X2;
	break;
	case PS3GPU_CTL_TILE_CMP_MODE_Z32_SEP:
		cmp_mode = PS3GPU_TILE_CMP_MODE_Z32_SEP;
	break;
	case PS3GPU_CTL_TILE_CMP_MODE_Z32_SEP_REG:
		cmp_mode = PS3GPU_TILE_CMP_MODE_Z32_SEP_REG;
	break;
	case PS3GPU_CTL_TILE_CMP_MODE_Z32_SEP_DIAG:
		cmp_mode = PS3GPU_TILE_CMP_MODE_Z32_SEP_DIAG;
	break;
	case PS3GPU_CTL_TILE_CMP_MODE_Z32_SEP_ROT:
		cmp_mode = PS3GPU_TILE_CMP_MODE_Z32_SEP_ROT;
	break;
	default:
		return (EINVAL);
	}

	if (req->tile_id < 0 ||
	    req->tile_id >= PS3GPU_CONTEXT_MAX_TILES)
		return (EINVAL);

	handle = PS3GPU_MEMORY_MAP_HANDLE(req->offset);
	offset = PS3GPU_MEMORY_MAP_OFFSET(req->offset);

	mmap = ps3gpu_dev_get_mmap(ps3gpu_dev, handle);
	if (!mmap)
		return (EINVAL);

	mem = mmap->mm_mem;
	context = mem->m_context;

	KASSERT(context[0] != NULL, ("invalid context"));

	if (req->context_id != context->ctx_id)
		return (EINVAL);

	if (mem->m_type != PS3GPU_MEMORY_TYPE_VIDEO &&
	    mem->m_type != PS3GPU_MEMORY_TYPE_GART)
		return (EINVAL);

	err = ps3gpu_tile_set(context, req->tile_id,
	    mem->m_type, req->size, req->pitch, cmp_mode, req->bank,
	    req->base, mem->m_start + offset);
	if (err)
		return (err);

	return (0);
}

static int
ps3gpu_ctl_tile_unset(struct ps3gpu_user *user,
    struct ps3gpu_ctl_tile_unset *req)
{
	struct ps3gpu_context *context;
	struct ps3gpu_memory *mem;
	struct ps3gpu_memory_map *mmap;
	unsigned long handle, offset;
	int err;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	if (req->tile_id < 0 ||
	    req->tile_id >= PS3GPU_CONTEXT_MAX_TILES)
		return (EINVAL);

	handle = PS3GPU_MEMORY_MAP_HANDLE(req->offset);
	offset = PS3GPU_MEMORY_MAP_OFFSET(req->offset);

	mmap = ps3gpu_dev_get_mmap(ps3gpu_dev, handle);
	if (!mmap)
		return (EINVAL);

	mem = mmap->mm_mem;
	context = mem->m_context;

	KASSERT(context != NULL, ("invalid context"));

	if (req->context_id != context->ctx_id)
		return (EINVAL);

	if (mem->m_type != PS3GPU_MEMORY_TYPE_VIDEO &&
	    mem->m_type != PS3GPU_MEMORY_TYPE_GART)
		return (EINVAL);

	err = ps3gpu_tile_unset(context, req->tile_id, mem->m_type,
	    req->bank, mem->m_start + offset);
	if (err)
		return (err);

	return (0);
}

static int
ps3gpu_ctl_cursor_initialize(struct ps3gpu_user *user,
    struct ps3gpu_ctl_cursor_initialize *req)
{
	struct ps3gpu_context *context;
	int head;
	int err;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	switch (req->head) {
	case PS3GPU_CTL_HEAD_A:
		head = PS3GPU_HEAD_A;
	break;
	case PS3GPU_CTL_HEAD_B:
		head = PS3GPU_HEAD_B;
	break;
	default:
		return (EINVAL);
	}

	context = ps3gpu_user_get_context(user, req->context_id);
	if (!context)
		return (EINVAL);

	err = ps3gpu_cursor_initialize(context, head);
	if (err)
		return (err);

	return (0);
}

static int
ps3gpu_ctl_cursor_set_image(struct ps3gpu_user *user,
    struct ps3gpu_ctl_cursor_set_image *req)
{
	struct ps3gpu_context *context;
	struct ps3gpu_memory *mem;
	struct ps3gpu_memory_map *mmap;
	int head;
	unsigned long handle, offset;
	int err;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	switch (req->head) {
	case PS3GPU_CTL_HEAD_A:
		head = PS3GPU_HEAD_A;
	break;
	case PS3GPU_CTL_HEAD_B:
		head = PS3GPU_HEAD_B;
	break;
	default:
		return (EINVAL);
	}

	handle = PS3GPU_MEMORY_MAP_HANDLE(req->offset);
	offset = PS3GPU_MEMORY_MAP_OFFSET(req->offset);

	mmap = ps3gpu_dev_get_mmap(ps3gpu_dev, handle);
	if (!mmap)
		return (EINVAL);

	mem = mmap->mm_mem;
	context = mem->m_context;

	KASSERT(context != NULL, ("invalid context"));

	if (req->context_id != context->ctx_id)
		return (EINVAL);

	err = ps3gpu_cursor_set_image(context, head, mem->m_start + offset);
	if (err)
		return (err);

	return (0);
}

static int
ps3gpu_ctl_cursor_set_position(struct ps3gpu_user *user,
    struct ps3gpu_ctl_cursor_set_position *req)
{
	struct ps3gpu_context *context;
	int head;
	int err;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	switch (req->head) {
	case PS3GPU_CTL_HEAD_A:
		head = PS3GPU_HEAD_A;
	break;
	case PS3GPU_CTL_HEAD_B:
		head = PS3GPU_HEAD_B;
	break;
	default:
		return (EINVAL);
	}

	context = ps3gpu_user_get_context(user, req->context_id);
	if (!context)
		return (EINVAL);

	err = ps3gpu_cursor_set_position(context, head, req->x, req->y);
	if (err)
		return (err);
	
	return (0);
}

static int
ps3gpu_ctl_cursor_enable(struct ps3gpu_user *user,
    struct ps3gpu_ctl_cursor_enable *req)
{
	struct ps3gpu_context *context;
	int head;
	int err;

	KASSERT(user != NULL, ("invalid user"));
	KASSERT(req != NULL, ("invalid request"));

	switch (req->head) {
	case PS3GPU_CTL_HEAD_A:
		head = PS3GPU_HEAD_A;
	break;
	case PS3GPU_CTL_HEAD_B:
		head = PS3GPU_HEAD_B;
	break;
	default:
		return (EINVAL);
	}

	context = ps3gpu_user_get_context(user, req->context_id);
	if (!context)
		return (EINVAL);

	err = ps3gpu_cursor_enable(context, head, req->enable);
	if (err)
		return (err);
	
	return (0);
}

static void
ps3gpu_user_dtr(void *data)
{
	struct ps3gpu_user *user = data;
	struct ps3gpu_context *context;
	struct ps3gpu_memory_map *mmap;

	KASSERT(user != NULL, ("invalid user"));

	sx_xlock(&user->u_lock);

	sx_xlock(&ps3gpu_dev->d_lock);

	while ((mmap = TAILQ_FIRST(&ps3gpu_dev->d_mmap_queue))) {
		TAILQ_REMOVE(&ps3gpu_dev->d_mmap_queue, mmap, mm_queue);
		free_unr(ps3gpu_dev->d_mmap_unrhdr,
		    mmap->mm_handle >> PS3GPU_MEMORY_MAP_HANDLE_SHIFT);
		free(mmap, M_PS3GPU);
	}

	sx_xunlock(&ps3gpu_dev->d_lock);

	while ((context = TAILQ_FIRST(&user->u_context_queue))) {
		TAILQ_REMOVE(&user->u_context_queue, context, ctx_user_queue);
		free(context->ctx_control_mem, M_PS3GPU);
		free(context->ctx_driver_info_mem, M_PS3GPU);
		free(context->ctx_reports_mem, M_PS3GPU);
		ps3gpu_context_free(context);
	}

	sx_xunlock(&user->u_lock);

	sx_destroy(&user->u_lock);

	free(user, M_PS3GPU);
}

static int
ps3gpu_open(struct cdev *dev, int oflags, int fmt,
    struct thread *td)
{
	struct ps3gpu_user *user;
	int err;

	if ((oflags & (FREAD | FWRITE)) != (FREAD | FWRITE))
		return (EINVAL);

	user = malloc(sizeof(*user), M_PS3GPU, M_WAITOK | M_ZERO);
	if (!user)
		return (ENOMEM);

	sx_init(&user->u_lock, "ps3gpu_user");

	TAILQ_INIT(&user->u_context_queue);

	err = devfs_set_cdevpriv(user, ps3gpu_user_dtr);
	if (err) {
		sx_destroy(&user->u_lock);
		free(user, M_PS3GPU);
		return (err);
	}

	return (0);
}

static int
ps3gpu_mmap(struct cdev *dev, vm_ooffset_t offset,
    vm_paddr_t *paddr, int nprot, vm_memattr_t *memattr)
{
	struct ps3gpu_user *user = NULL;
	struct ps3gpu_context *context;
	struct ps3gpu_memory *mem;
	struct ps3gpu_memory_map *mmap;
	unsigned long handle;

	if (nprot & PROT_EXEC)
		return (EINVAL);

	devfs_get_cdevpriv((void **) &user);

	handle = PS3GPU_MEMORY_MAP_HANDLE(offset);
	offset = PS3GPU_MEMORY_MAP_OFFSET(offset);

	mmap = ps3gpu_dev_get_mmap(ps3gpu_dev, handle);
	if (!mmap)
		return (EINVAL);

	mem = mmap->mm_mem;
	context = mem->m_context;

	KASSERT(context != NULL, ("invalid context"));

	if (user && context->ctx_user != user)
		return (EINVAL);

	if (offset >= mem->m_size)
		return (EINVAL);

	switch (mem->m_type) {
	case PS3GPU_MEMORY_TYPE_VIDEO:
		*paddr = mem->m_paddr + offset;
		*memattr = VM_MEMATTR_WRITE_COMBINING;
	break;
	case PS3GPU_MEMORY_TYPE_GART:
		*paddr = vtophys(mem->m_vaddr + offset);
		*memattr = VM_MEMATTR_DEFAULT;
	break;
	case PS3GPU_MEMORY_TYPE_REGISTER:
		*paddr = mem->m_paddr + offset;
		*memattr = VM_MEMATTR_UNCACHEABLE;
	break;
	}

	return (0);
}

static int
ps3gpu_ioctl(struct cdev *dev, u_long cmd, caddr_t data,
    int fflag, struct thread *td)
{
	struct ps3gpu_user *user;
	int err;

	err = devfs_get_cdevpriv((void **) &user);
	if (err)
		return (err);

	switch (cmd) {
	case PS3GPU_CTL_CONTEXT_ALLOCATE:
	{
		struct ps3gpu_ctl_context_allocate *req =
		    (struct ps3gpu_ctl_context_allocate *) data;

		err = ps3gpu_ctl_context_allocate(user, req);
	}
	break;
	case PS3GPU_CTL_CONTEXT_FREE:
	{
		struct ps3gpu_ctl_context_free *req =
		    (struct ps3gpu_ctl_context_free *) data;

		err = ps3gpu_ctl_context_free(user, req);
	}
	break;
	case PS3GPU_CTL_MEMORY_ALLOCATE:
	{
		struct ps3gpu_ctl_memory_allocate *req =
		    (struct ps3gpu_ctl_memory_allocate *) data;

		err = ps3gpu_ctl_memory_allocate(user, req);
	}
	break;
	case PS3GPU_CTL_MEMORY_FREE:
	{
		struct ps3gpu_ctl_memory_free *req =
		    (struct ps3gpu_ctl_memory_free *) data;

		err = ps3gpu_ctl_memory_free(user, req);
	}
	break;
	case PS3GPU_CTL_SETUP_CONTROL:
	{
		struct ps3gpu_ctl_setup_control *req =
		    (struct ps3gpu_ctl_setup_control *) data;

		err = ps3gpu_ctl_setup_control(user, req);
	}
	break;
	case PS3GPU_CTL_SET_FLIP_MODE:
	{
		struct ps3gpu_ctl_set_flip_mode *req =
		    (struct ps3gpu_ctl_set_flip_mode *) data;

		err = ps3gpu_ctl_set_flip_mode(user, req);
	}
	break;
	case PS3GPU_CTL_RESET_FLIP_STATUS:
	{
		struct ps3gpu_ctl_reset_flip_status *req =
		    (struct ps3gpu_ctl_reset_flip_status *) data;

		err = ps3gpu_ctl_reset_flip_status(user, req);
	}
	break;
	case PS3GPU_CTL_FLIP:
	{
		struct ps3gpu_ctl_flip *req =
		    (struct ps3gpu_ctl_flip *) data;

		err = ps3gpu_ctl_flip(user, req);
	}
	break;
	case PS3GPU_CTL_DISPLAY_BUFFER_SET:
	{
		struct ps3gpu_ctl_display_buffer_set *req =
		    (struct ps3gpu_ctl_display_buffer_set *) data;

		err = ps3gpu_ctl_display_buffer_set(user, req);
	}
	break;
	case PS3GPU_CTL_DISPLAY_BUFFER_UNSET:
	{
		struct ps3gpu_ctl_display_buffer_unset *req =
		    (struct ps3gpu_ctl_display_buffer_unset *) data;

		err = ps3gpu_ctl_display_buffer_unset(user, req);
	}
	break;
	case PS3GPU_CTL_DISPLAY_BUFFER_FLIP:
	{
		struct ps3gpu_ctl_display_buffer_flip *req =
		    (struct ps3gpu_ctl_display_buffer_flip *) data;

		err = ps3gpu_ctl_display_buffer_flip(user, req);
	}
	break;
	case PS3GPU_CTL_TILE_SET:
	{
		struct ps3gpu_ctl_tile_set *req =
		    (struct ps3gpu_ctl_tile_set *) data;

		err = ps3gpu_ctl_tile_set(user, req);
	}
	break;
	case PS3GPU_CTL_TILE_UNSET:
	{
		struct ps3gpu_ctl_tile_unset *req =
		    (struct ps3gpu_ctl_tile_unset *) data;

		err = ps3gpu_ctl_tile_unset(user, req);
	}
	break;
	case PS3GPU_CTL_CURSOR_INITIALIZE:
	{
		struct ps3gpu_ctl_cursor_initialize *req =
		    (struct ps3gpu_ctl_cursor_initialize *) data;

		err = ps3gpu_ctl_cursor_initialize(user, req);
	}
	break;
	case PS3GPU_CTL_CURSOR_SET_IMAGE:
	{
		struct ps3gpu_ctl_cursor_set_image *req =
		    (struct ps3gpu_ctl_cursor_set_image *) data;

		err = ps3gpu_ctl_cursor_set_image(user, req);
	}
	break;
	case PS3GPU_CTL_CURSOR_SET_POSITION:
	{
		struct ps3gpu_ctl_cursor_set_position *req =
		    (struct ps3gpu_ctl_cursor_set_position *) data;

		err = ps3gpu_ctl_cursor_set_position(user, req);
	}
	break;
	case PS3GPU_CTL_CURSOR_ENABLE:
	{
		struct ps3gpu_ctl_cursor_enable *req =
		    (struct ps3gpu_ctl_cursor_enable *) data;

		err = ps3gpu_ctl_cursor_enable(user, req);
	}
	break;
	default:
		err = ENOIOCTL;
	break;
	}

	return (err);
}

static int
ps3gpu_device_init(struct ps3gpu_device **pdev)
{
	struct ps3gpu_device *dev;

	dev = malloc(sizeof(*dev), M_PS3GPU, M_WAITOK | M_ZERO);
	if (!dev)
		return (ENOMEM);

	sx_init(&dev->d_lock, "ps3gpu_device");

	TAILQ_INIT(&dev->d_mmap_queue);

	dev->d_mmap_unrhdr = new_unrhdr(1,
	    ((1 << PS3GPU_MEMORY_MAP_HANDLE_BITS) - 1), NULL);
	if (!dev->d_mmap_unrhdr) {
		sx_destroy(&dev->d_lock);
		free(dev, M_PS3GPU);
		return (ENOMEM);
	}

	*pdev = dev;

	return (0);
}

static void
ps3gpu_device_fini(struct ps3gpu_device *dev)
{
	int i;

	for (i = 0; i < PS3GPU_DEVICE_MAX_CONTEXTS; i++) {
		if (dev->d_context[i])
			ps3gpu_context_free(dev->d_context[i]);
	}

	delete_unrhdr(dev->d_mmap_unrhdr);

	sx_destroy(&dev->d_lock);

	free(dev, M_PS3GPU);
}

static struct ps3gpu_memory_map *
ps3gpu_dev_get_mmap(struct ps3gpu_device *dev,
    unsigned long handle)
{
	struct ps3gpu_memory_map *mmap;

	sx_xlock(&dev->d_lock);

	TAILQ_FOREACH(mmap, &dev->d_mmap_queue, mm_queue) {
		if (mmap->mm_handle == handle) {
			sx_xunlock(&dev->d_lock);
			return (mmap);
		}
	}

	sx_xunlock(&dev->d_lock);

	return (NULL);
}

static unsigned long
ps3gpu_dev_alloc_handle(struct ps3gpu_device *dev)
{
	unsigned long handle;

	sx_xlock(&dev->d_lock);

	handle = (unsigned long) alloc_unr(dev->d_mmap_unrhdr) <<
	    PS3GPU_MEMORY_MAP_HANDLE_SHIFT;

	sx_xunlock(&dev->d_lock);

	return (handle);
}

static void
ps3gpu_dev_free_handle(struct ps3gpu_device *dev,
    unsigned long handle)
{
	sx_xlock(&dev->d_lock);

	free_unr(dev->d_mmap_unrhdr,
	    handle >> PS3GPU_MEMORY_MAP_HANDLE_SHIFT);

	sx_xunlock(&dev->d_lock);
}


static int
ps3gpu_modevent(module_t mod, int type, void *arg)
{
	int err;

	switch (type) {
	case MOD_LOAD:
		/* Create GPU device */

		err = ps3gpu_device_init(&ps3gpu_dev);
		if (err) {
			printf("ps3gpu: Failed to create GPU device\n");
			return (err);
		}

		/* Create char device */

		ps3gpu_cdev = make_dev(&ps3gpu_cdevsw, 0,
		    UID_ROOT, GID_WHEEL, 0600, "ps3gpu");
		if (!ps3gpu_cdev) {
			printf("ps3gpu: Failed to create /dev/ps3gpu\n");
			ps3gpu_device_fini(ps3gpu_dev);
			return (ENOMEM);
		}
	break;
	case MOD_UNLOAD:
		/* Destroy char device */

		if (ps3gpu_cdev)
			destroy_dev(ps3gpu_cdev);

		if (ps3gpu_dev)
			ps3gpu_device_fini(ps3gpu_dev);
	break;
	case MOD_SHUTDOWN:
	break;
	default:
		return (EOPNOTSUPP);
	}

	return (0);
}

DEV_MODULE(ps3gpu, ps3gpu_modevent, NULL);
MODULE_VERSION(ps3gpu, 1);
