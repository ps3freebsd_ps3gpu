/*-
 * Copyright (C) 2011 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#ifndef _PS3GPU_HEAP_H
#define _PS3GPU_HEAP_H

struct ps3gpu_heap_block {
	TAILQ_ENTRY(ps3gpu_heap_block) hb_queue;

	int hb_start;
	int hb_size;
	int hb_free;
};

TAILQ_HEAD(ps3gpu_heap_block_queue, ps3gpu_heap_block);

struct ps3gpu_heap {
	int h_size;

	struct ps3gpu_heap_block_queue h_block_queue;
};

int ps3gpu_heap_init(struct ps3gpu_heap *h, int size);

void ps3gpu_heap_fini(struct ps3gpu_heap *h);

int ps3gpu_heap_allocate(struct ps3gpu_heap *h, int size, int align,
    int *start);

int ps3gpu_heap_free(struct ps3gpu_heap *h, int start, int size);

#endif /* _PS3GPU_HEAP_H */
